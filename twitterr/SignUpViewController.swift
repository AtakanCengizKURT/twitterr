//
//  SignUpViewController.swift
//  twitterr
//
//  Created by Atakan Cengiz KURT on 8.05.2017.
//  Copyright © 2017 Atakan Cengiz KURT. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passField: UITextField!
    @IBOutlet weak var passAgainField: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func SignUp(_ sender: Any) {
        if(passField.text != passAgainField.text)
        {
            print("Şifre tekrarları uuşmuyor")
        }else if(!(emailField.text?.isEmpty)! && !(passField.text?.isEmpty)! && !(passAgainField.text?.isEmpty)!)
        {
            
            FIRAuth.auth()?.createUser(withEmail: emailField.text!, password: passField.text!, completion: { (user, error) in
                if(error != nil) //HATA boş değilse
                {
                    print(error!.localizedDescription)
                }else
                {
                    print("*******Kaydınız başarı ile gerçekleşti")
                    self.emailField.text = ""
                    self.passField.text = ""
                    self.passAgainField.text = ""
                }
            })
            
        }
    }
   
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
