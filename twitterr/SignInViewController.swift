//
//  SignInViewController.swift
//  twitterr
//
//  Created by Atakan Cengiz KURT on 10.05.2017.
//  Copyright © 2017 Atakan Cengiz KURT. All rights reserved.
//

import UIKit
import Firebase

class SignInViewController: UIViewController {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func signIn(_ sender: Any) {
        if(!(emailField.text?.isEmpty)! && !(passField.text?.isEmpty)!)
        {
        FIRAuth.auth()?.signIn(withEmail: self.emailField.text!, password: self.passField.text!, completion: { (user, error) in
            if(error != nil)
            {
            print(error!)
            }
            else
            {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeViewController
                self.present(homeVC, animated: true, completion: nil)
                
            }
        })
        }
        else
        {
        alarm()
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func alarm()
    {
        let alert = UIAlertController(title: "HATA", message: "Lütfen boş alan bırakmayın", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "TAMAM", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

}
